<?php
namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use AppBundle\Entity\Personnes;

class ControllerTransport extends Controller {
/**
* @Route("/create-list")
*/

public function detaildep(Request $request) {

  $Personnes = new Personnes();
  $form = $this->createFormBuilder($Personnes)
    ->add('nom', TextType::class)
    ->add('prenom', TextType::class)
    ->add('email', EmailType::class)
    ->add('ville', ChoiceType::class, [
    'choices' => [
            'Tunis'=> 'Tunis',
            'Sousse'=> 'Sousse',
            'Sfax'=> 'Sfax',
            'Djerba'=> 'Djerba',
        ],
])
    ->add('moyen', ChoiceType::class,array(
                'choices'  => array(
                    'avion' => 0,
                    'voiture' => 1,
                    'metro' => 2,
                ),
                'expanded' => true,
                'multiple' => false
            ))
    ->add('datededepart', DateType::class)
    ->add('heurededepart', TimeType::class)
    ->add('datederetour', DateType::class)
    ->add('heurederetour', TimeType::class)
    ->add('save', SubmitType::class, array('label' => 'Enregistrer'))
    ->add('reset', ResetType::class, array('label' => 'Supprimer'))
    ->getForm();

  $form->handleRequest($request);

  if ($form->isSubmitted()) {

    $deplacement = $form->getData();

    $em = $this->getDoctrine()->getManager();
    $em->persist($Personnes);
    $em->flush();

   return $this->redirect('/view-article/' . $Personnes->getId());

  }

  return $this->render(
    'deplacement/edit.html.twig',
    array('form' => $form->createView())
    );

}
/**
* @Route("/view-details/{id}")
*/   
public function viewAction($id) {

  $article = $this->getDoctrine()
    ->getRepository('AppBundle:Article')
    ->find($id);

  if (!$Personnes) {
    throw $this->createNotFoundException(
    'There are no details with the following id: ' . $id
    );
  }

  return $this->render(
    'departement/view.html.twig',
    array('Personnes' => $Personnes)
    );

}

/**
* @Route("/update-deplacement/{id}")
*/  
public function updatedep(Request $request, $id) {

  $em = $this->getDoctrine()->getManager();
  $Personnes = $em->getRepository('AppBundle:Personnes')->find($id);

  if (!$Personnes) {
    throw $this->createNotFoundException(
    'There are no deplacement with the following id: ' . $id
    );
  }

  $form = $this->createFormBuilder($Personnes)
    ->add('nom', TextType::class)
    ->add('prenom', TextType::class)
    ->add('email', EmailType::class)
    ->add('ville', ChoiceType::class, [
    'choices' => [
            'Tunis'=> 'Tunis',
            'Sousse'=> 'Sousse',
            'Sfax'=> 'Sfax',
            'Djerba'=> 'Djerba',
        ],
])
    ->add('datededepart', DateType::class)
    ->add('heurededepart', TimeType::class)
    ->add('datederetour', DateType::class)
    ->add('heurederetour', TimeType::class)
    ->add('moyen', TextType::class)
    ->add('save', SubmitType::class, array('label' => 'Update'))
    ->getForm();

  $form->handleRequest($request);

  if ($form->isSubmitted()) {

    $Personnes = $form->getData();
    $em->flush();

    return $this->redirect('/create-list');

  }

  return $this->render(
    'deplacement/edit.html.twig',
    array('form' => $form->createView())
    );

}
}
